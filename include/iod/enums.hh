#pragma once

#include <cstring>
#include <ostream>
#include <stdexcept>
#include <string_view>
#include <unordered_map>

namespace iod {

    template<typename E>
    requires std::is_enum_v<E>

    struct EnumValue {
        EnumValue() = default;

        EnumValue(E e, const char* name)
                : Name{name},
                  mValue{e},
                  mValid{true} {}

        E operator*() const {
            if (!mValid) {
                throw std::runtime_error("Cannot read enum value, currently invalid");
            }
            return mValue;
        }

        const char* Name{"--NotValid--"};

        operator bool() const { return mValid; }

    private:
        E mValue;
        bool mValid{false};
    };

    template<typename E>
    using EnumValues = std::unordered_map<E, EnumValue<E>>;

    template<typename E>
    struct EnumMeta {
        const char* Name;
        EnumValues<E> Values;
        bool EncodeStrings{true};

        const EnumValue<E>& operator[](E e) const { return Values.find(e)->second; }

        EnumValue<E> operator[](const std::string_view& name) const {
            auto found = std::find_if(Values.begin(), Values.end(),
                              [&name](const auto& it) {
                                std::string_view tmp{it.second.Name};
                                return (name.size() == tmp.size()) &&
                                       (strncasecmp(tmp.data(), name.data(), name.size()) == 0);
                              });
            if (found == Values.end()) {
                return EnumValue<E>{};
            }
            return found->second;
        }
    };

    template <typename E>
    struct is_meta_enum : std::false_type {};

    template <typename E>
    constexpr bool is_meta_enum_v = is_meta_enum<E>::value;

    template <typename T>
    concept MetaEnum = is_meta_enum_v<T>;

#define EnumEntry(type, name) { type :: name, {type :: name, #name}}

    template<MetaEnum E>
    const EnumMeta<E>& Meta();

    template<MetaEnum E>
    const EnumValue<E>& Enum(E e) {
        return Meta<E>()[e];
    }

    template<MetaEnum E>
    EnumValue<E> Enum(const std::string_view& name) {
        return Meta<E>()[name];
    }
}

template <iod::MetaEnum E>
std::ostream& operator<<(std::ostream& os, E e)
{
    return (os << iod::Enum(e).Name);
}

template <iod::MetaEnum E>
std::ostream& operator<<(std::ostream& os, const iod::EnumValue<E>& e)
{
    if (e) {
        return (os << int(*e) << "_" << e.Name);
    }
    else {
        return (os << iod::Meta<E>().Name << "::" << e.Name);
    }
}