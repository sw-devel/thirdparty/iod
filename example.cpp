//
// Created by Mpho Mbotho on 2021-07-07.
//
#include "include/iod/enums.hh"
#include "include/iod/json.hh"

#include <iostream>

enum Color {
    Red,
    Green,
    Blue
};

namespace iod {
    template<>
    struct is_meta_enum<Color> : std::true_type {};
    template<>

    const iod::EnumMeta<Color>& Meta<Color>()
    {
        static const EnumMeta<Color> sColorMeta = {
            "Color",
            {
                    EnumEntry(Color, Red),
                    EnumEntry(Color, Green),
                    EnumEntry(Color, Blue)
            },
            false
        };
        return sColorMeta;
    }
}

enum Stage {
    Start,
    Move,
    End
};

iod_define_symbol(first);
iod_define_symbol(second);

typedef decltype(iod::D(
        s::_first = Color(),
        s::_second = Stage()
)) Object;

int main(int argc, char *argv[])
{
    std::cout << "Is Meta Enum? " <<  iod::is_meta_enum_v<Color>;
    std::cout << "\n Meta Name: " << iod::Meta<Color>().Name;
    std::cout << "\n Find by value: " << iod::Enum(Color::Green).Name;
    std::cout << "\n Find by name: " << iod::Enum<Color>("Blue").Name;
    std::cout << "\n Find by name: " << iod::Enum<Color>("gREEn");
    std::cout << "\n Find by name: " << iod::Enum<Color>("gREEnEr");

    Object obj;
    obj.first = Color::Blue;
    obj.second = Stage::End;
    auto str = iod::json_encode(obj);
    std::cout << "\n " << str;
    Object obj2;
    iod::json_decode(obj2, str);
    std::cout << "\n Decoded: " << obj2.first << ", " << int(obj2.second) << "\n";
    std::cout << iod::has_symbol<void, s::_first_t>::value;
    return EXIT_SUCCESS;
}