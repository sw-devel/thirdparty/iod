##
# file with utility macros
##

##
# @brief generate IOD symbols
##
function(suil_iod_symbols name)
    set(options "")
    set(kvargs  BINARY OUTPUT PROJECT)
    set(kvvargs DEPENDS SYMBOLS)

    cmake_parse_arguments(IOD_SYMBOLS "${options}" "${kvargs}" "${kvvargs}" ${ARGN})

    # locate binary to use
    set(iodsyms iodsyms)
    if (IOD_SYMBOLS_BINARY)
        set(iodsyms ${IOD_SYMBOLS_BINARY})
    endif()

    # locate symbols file
    set(${name}_SYMBOLS ${CMAKE_SOURCE_DIR}/${name}.sym)
    if (IOD_SYMBOLS_SYMBOLS)
        set(${name}_SYMBOLS ${IOD_SYMBOLS_SYMBOLS})
    endif()

    # set output path
    set(${name}_OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/${name}_symbols.h)
    if (IOD_SYMBOLS_OUTPUT)
        set(${name}_OUTPUT ${IOD_SYMBOLS_OUTPUT})
    endif()

    list(GET ${name}_SYMBOLS 0 ${name}_MAIN_SYM)
    list(REMOVE_AT ${name}_SYMBOLS 0)
    set(${name}_FLAT_SYMS ${${name}_MAIN_SYM})
    foreach(__${name}_SYM ${${name}_SYMBOLS})
        set(${name}_FLAT_SYMS "${${name}_FLAT_SYMS} ${__${name}_SYM}")
    endforeach()

    message(STATUS "${name} symbols. main: ${${name}_MAIN_SYM} flat: ${${name}_FLAT_SYMS}")

    add_custom_command(OUTPUT ${${name}_OUTPUT}
            COMMAND ${iodsyms} ${${name}_MAIN_SYM} ${${name}_SYMBOLS} ${${name}_OUTPUT}
            WORKING_DIRECTORY  ${CMAKE_BINARY_DIR}
            DEPENDS            ${${name}_MAIN_SYM} ${${name}_SYMBOLS})
    # add symbol generation target
    add_custom_target(${name}-gensyms
            DEPENDS            ${${name}_OUTPUT}
            COMMENT            "Generating IOD symbols used by ${name}")
    message(STATUS "${iodsyms} ${${name}_FLAT_SYMS} ${${name}_OUTPUT}")

    if (IOD_SYMBOLS_DEPENDS)
        add_dependencies(${name}-gensyms ${IOD_SYMBOLS_DEPENDS})
    endif()

    if (NOT IOD_SYMBOLS_PROJECT)
        add_dependencies(${name} ${name}-gensyms)
    endif()
endfunction()